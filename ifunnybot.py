from http.client import HTTPSConnection
import os
import telebot
from bs4 import BeautifulSoup
import requests
import re
from dotenv import load_dotenv
import datetime as _dt

a = True
while a:
    print("starting loop")
    try:
        load_dotenv()
        TOKEN = os.getenv('API_KEY')
        bot = telebot.TeleBot(TOKEN)

        def link_extractor(text):
            m = re.search("https:\/\/ifunny\.co\/video\/[A-Za-z0-9\_\-]+", text)
            if m is not None:
                print("text: ", text, "\n", m.group(0))
                return m.group(0)
            else:
                return "link extractor failed on string"


        def scraper(message):
            headers = {
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36',
            }
            url = link_extractor(message.text)
            html_doc = requests.get(url, headers=headers)
            soup = BeautifulSoup(html_doc.content.decode('utf-8'), 'html.parser')
            str_soup = str(soup)
            m = re.search('https:\/\/[A-Za-z0-9_-]+\.ifunny\.co\/videos\/[A-Za-z0-9_-]+\.mp4', str_soup)
            if m is not None:
                return m.group(0)
            else:
                print("url: ", url, "\n")
                return "generic scraper error"

        @bot.message_handler(regexp="https:\/\/ifunny\.co\/video\/[A-Za-z0-9\_\-]+")

        def send_content(message):
            bot.reply_to(message, scraper(message))

        bot.polling()

    except (BaseException, ) as e:
        print(f'recieved error at {_dt.datetime.utcnow()}, {e}')
    except (BaseException, ) as e:
        a = False
print('all done')
from bs4 import BeautifulSoup
import requests
import re

headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36',
}

url = "https://ifunny.co/video/ZE0sRwZE9?s=cl"
html_doc = requests.get(url, headers=headers)
soup = BeautifulSoup(html_doc.content.decode('utf-8'), 'html.parser')
str_soup = str(soup)
m = re.search('https:\/\/img\.ifunny\.co\/videos\/[A-Za-z0-9\_]+\.mp4', str_soup)
if m is not None:
    print(m.group(0))
else:
    print("maybe ifunny changed their site")
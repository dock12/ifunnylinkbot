notes.md

put the below in ~/.gitconfig  git passwords are stored in ~/.git-credentials  

```yaml
[user]
	name = bilbo
	email = jpartridge36@protonmail.com
[credential]
  helper = cache --timeout=360000
[credential "https://gitlab.com"]
  username = bilbo
```